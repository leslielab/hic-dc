# README #

_____________________________________________________________________________________
Created by Mark Carty
August 7th, 2015
_____________________________________________________________________________________
 
HiC-DC is a tool for assigning p-value to intra-chromosomal contact maps produced
by genome contact assays.

_____________________________________________________________________________________
HOW TO INSTALL DEPENDENCIES

R version 3.2.1 with the packages below to installed:

	1.	pscl
	2.	VGAM
	3.	methods
	4.	data.table
	5.	DBI
	6.	RSQLite
______________________________________________________________________________________

HOW TO RUN THE SOFTWARE

To simply run this script type

     Rscript ../HiCDC.R

usage: HiCDC.R [options]

arguments to script: 

	1.	file with Hi-C covariates and Hi-C counts
	2.	chromosome
	3.	min linear distance between locus pairs, default 0 bps
	4.	max linear distance between locus pairs, default 2 Mb
	5.	number of knots, default = 5
	6.	sample size, default = 0.1

Run the script as follows:

    Rscript …/HiCDC.R Desktop/hIMR90_HindIII_Features_chr1.rds chr1 0 2000000 5 0.1
_________________________________________________________________________________________

HOW TO PREPARE INPUT FILE FOR HiC-DC

HiC-DC requires one input file.  As long as these files are provided, HiC-DC can be applied on data sets 
from different Hi-C experiments using a resolution that is dependent on restriction fragments. 

For R version 3.2.1, the following packages should be installed:

	a.	diffHic
	b.	GenomicAlignments
	c.	GenomicRanges
	d.	BSgenome
	e.	BSgenome.Hsapiens.UCSC.hg19
	f.	doMC


If the above-mentioned packages are installed, R scripts RunHiC.R and constructFeatures.R can be use to generate Hi-C covariates and counts.
We have modified the Python script presplit_map.py from diffHic package to include the bwa-mem aligner.

HOW TO RUN THE SOFTWARE

Usage: presplit_map2.py [options]

     -G        Path to aligner genome index, prefix only
     -1        Path to FASTQ file containing the first read of the pair
     -2        Path to FASTQ file containing the second read of the pair
     -o        Path to output BAM file
    —-sig      Sequence of ligation signature
    —-cmd      Aligner
     
Run the python script presplit_map2.py as follows:

     python presplit_map2.py -G genome -1 SRR027956_1.fastq -2 SRR027956_2.fastq —-cmd bowtie2 —-sig AAGCTAGCTT -o SRR027956.bam

After aligning the FASTQ files, perform a diagnostic assessment and filter Hi-C artifacts using diffHic package 


     > library(diffHic)
     > library(BSgenome.Hsapiens.UCSC.hg19)
     > ms.frag <- cutGenome(BSgenome.Hsapiens.UCSC.hg19,"GATC",4)
     > chrom <- sprintf('chr%s',c(1:22,"M","X","Y"))
     > idx <- unlist(sapply(chrom,function(x) which(seqnames(ms.frag)==x)))
     > ms.frag <- ms.frag[idx]
     > ms.frag
       GRanges object with 7127634 ranges and 0 metadata columns:
                seqnames               ranges strand
                  <Rle>            <IRanges>  <Rle>
           [1]     chr1       [    1, 11163]      *
           [2]     chr1       [11160, 12414]      *
           [3]     chr1       [12411, 12464]      *
           [4]     chr1       [12461, 12689]      *
           [5]     chr1       [12686, 12832]      *
           ...      ...                  ...    ...
     [7127630]     chrY [59360039, 59360267]      *
     [7127631]     chrY [59360264, 59360317]      *
     [7127632]     chrY [59360314, 59360727]      *
     [7127633]     chrY [59360724, 59361569]      *
     [7127634]     chrY [59361566, 59373566]      *
     -------
     seqinfo: 93 sequences from hg19 genome
     >
     > ms.param <- pairParam(ms.frag)
     > ms.param

     Genome contains 7127634 restriction fragments across 25 chromosomes
     No discard regions are specified
     No limits on chromosomes for read extraction
     No cap on the read pairs per pair of restriction fragments

     > diagnostic.bwa <- preparePairs("SRR1658582_bwa.bam",ms.param,file="SRR1658582_bwa.h5",dedup=TRUE,minq=10)
     > diagnostic.bwa
     $pairs
     total   marked filtered   mapped 

     55817881  0    28075321   27742560

     $same.id

     dangling self.circle 

     971337       12779 

    $singles
    [1] 0

    $chimeras

    total  mapped   multi invalid 

    5193538      10       9       5


Use the prunePairs() function from the diffHic package to filter Hi-C artifacts.

      library(rhdf5)
      library(GenomicRanges)
      chrom <- sprintf('chr%s',c(1:22,"X","Y"))
      options(warn=-1)
      R1 <- GRanges()
      R2 <- GRanges()
      for(chr in chrom){
  
          data <- h5read('…/SRR027956_trimmed.h5',name=chr)
          dat <- data[[1]]

          R1 <- c(R1,GRanges(seqnames=chr,IRanges(start=dat$target.pos,end=dat$target.pos + abs(dat$target.len)),strand=ifelse(dat$target.len > 0,"+","-")))
          R2 <- c(R2,GRanges(seqnames=chr,IRanges(start=dat$anchor.pos,end=dat$anchor.pos + abs(dat$anchor.len)),strand=ifelse(dat$anchor.len > 0,"+","-")))
  
     }
     reads <- GRangesList(R1=R1,R2=R2)
     > reads
     GRangesList object of length 2:
     $R1 
     GRanges object with 388715 ranges and 0 metadata columns:
                 seqnames                 ranges strand
                   <Rle>              <IRanges>  <Rle>
          [1]     chr1     [ 665078,  665154]      +
          [2]     chr1     [ 783713,  783789]      -
          [3]     chr1     [ 772357,  772433]      +
          [4]     chr1     [ 799743,  799819]      +
          [5]     chr1     [1049304, 1049380]      -
          ...      ...                    ...    ...
     [388711]     chrY [101313820, 101313896]      +
     [388712]     chrY [166559995, 166560071]      -
     [388713]     chrY [242831561, 242831637]      -
     [388714]     chrY [ 89017082,  89017158]      -
     [388715]     chrY [169880341, 169880417]      +

      ...
     <1 more element>
     -------
     seqinfo: 24 sequences from an unspecified genome; no seqlengths
     > 
We can now use RunHiC.R and constructFeatures.R to get Hi-C covariates and Hi-C counts.